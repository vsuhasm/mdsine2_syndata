import pickle as pkl
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

import util
import model


parameters = util.Parameters(
    # load_from_pickle=None,
    seed=2,
    n_cpus=10,
    burnin=0,
    n_samples=10,
    n_time_points=None,
    diagnostic_variables=['n_clusters'],
    rescale_qpcr_data=1e4,
    prior_var_self_interactions_prior_scale=1e-4,
    prior_var_interactions_prior_scale=1e-4,
    process_var_prior_scale=1e4,
    qpcr_dada_data_folder='../raw_data')

with open('mouse_set.pkl', 'rb') as fp:
        s = pkl.load(fp)

s.filter_consistency(
    min_rel_abundance=0.05,
    min_num_subjects=3,
    min_num_counts=10,
    min_num_consecutive=3)



if parameters.MOUSE_SET == 1:
    s = s.mouse_set1
else:
    s = s.mouse_set2

print(len(s.data_tables.otus))

if parameters.RESCALE_QPCR_DATA is not None:
    s.rescale_qpcr(max_value=parameters.RESCALE_QPCR_DATA)

data = s.mice['2'].matrix(add_min_rel_abundance=False, include_nan=False)
data_raw_counts = data['raw']
data_abs_abund = data['abs']
data_rel_abund = data['rel']
print(data_raw_counts.shape, data_abs_abund.shape, data_rel_abund.shape)

df_raw_counts = pd.DataFrame(data_raw_counts)
df_abs_abund = pd.DataFrame(data_abs_abund)
df_rel_abund = pd.DataFrame(data_rel_abund)

df_raw_counts.to_csv('./MID_2_counts.csv', index=False)
df_abs_abund.to_csv('./MID_2_abs_abund.csv', index=False)
df_rel_abund.to_csv('./MID_2_rel_abund.csv', index=False)